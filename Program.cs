﻿using System;

namespace _24._03_Car_Abstract_class
{
    public abstract class Car
    {
        public string type;
        public string mark;
        public float force;
        public string model;
        public float mileage;

        public Car(string mark, string model, float force, float mileage)
        {
            this.mark = mark;
            this.model = model;
            this.force =force;
            this.mileage = mileage;
        }

        public abstract double CalcTax();
    }

    public class PassengerCar : Car
    {
        public PassengerCar(string mark, string model, float force, float mileage)
        : base(mark, model, force, mileage)
        {
            type = "passenger car";
        } 
    
    
                public override double CalcTax() 
        {
            if (force <= 100)
                return force * 2.5;
            else if (force > 100 && force <= 150)
                return force * 3.5;
            else if (force > 150 && force <= 200)
                return force * 5;
            else if (force > 200 && force <= 250)
                return force * 7.5;
            else if (force > 250)
                return force * 15;
            else
                return 0;
        }
    }public class Bus : Car
    {
        public Bus(string mark, string model, float force, float mileage)
        : base(mark, model, force, mileage)
        {
            type = "Bus";
        }
        public override double CalcTax()
        {
            if (force <= 200)
                return force * 5;
            else if (force > 200)
                return force * 10;
            else
                return 0;
        }
    }
            public class Snowmobile : Car
            {
                public Snowmobile(string mark, string model, float force, float mileage)
                : base(mark, model, force, mileage)
                {
                    type = "Snowmobile";
                }
            public override double CalcTax()
            {
                if (force < 50)
                    return force * 2.5;
                else if (force > 50)
                    return force * 5;
                else
                    return 0;
            }
    }
}
