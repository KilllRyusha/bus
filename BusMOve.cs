﻿using UnityEngine;

public class BusMOve : MonoBehaviour
{
    private Vector3 start;
    private Vector3 finish;
    private Vector3 Acc;
    [SerializeField] private Transform start1;
    [SerializeField] private Transform finish1;
    private Vector3 start22;
    private Vector3 finish22;
    private Vector3 start33;
    private Vector3 finish33;
    [SerializeField] private Transform start2;
    [SerializeField] private Transform start3;

    [SerializeField] private Transform finish2;
    [SerializeField] private Transform finish3;
    bool GotoFin = true;
    [SerializeField]
    private float dif = 0.1f;
    [SerializeField] private float Speed = 10;
    [SerializeField]
    private float acc = 2f;
    [SerializeField]
    private Transform Accpos;
    Transform currentBus;
    private int i = 0;
    int canMove = 0;
    void Start()
    {
        Acc = Accpos.transform.position;
        finish = finish1.transform.position;
        start = start1.transform.position;
        start22 = start2.transform.position;
        finish22 = finish2.transform.position;
        start33 = start3.transform.position;
        finish33 = finish3.transform.position;
    }

    void Update()
    {
        currentBus = BussPool.Instance.BusPool[i].transform;
        if (Vector3.Distance(transform.position, Acc) < acc)
        {
            canMove = 1;
        }

        if (canMove == 1)
        {
            if (Vector3.Distance(transform.position, finish) > dif && GotoFin == true)
            {
                transform.Translate(0, 0, Speed * Time.deltaTime);

            }
            else GotoFin = false;
            if (Vector3.Distance(transform.position, start) > dif && GotoFin == false)
            {
                transform.Translate(0, 0, -Speed * Time.deltaTime);

            }
            else GotoFin = true;
            if (Vector3.Distance(transform.position, finish22) > dif && GotoFin == true)
            {
                transform.Translate(0, 0, Speed * Time.deltaTime);

            }
            else GotoFin = false;
            if (Vector3.Distance(transform.position, start22) > dif && GotoFin == false)
            {
                transform.Translate(0, 0, -Speed * Time.deltaTime);

            }
            else GotoFin = true;
            if (Vector3.Distance(transform.position, finish33) > dif && GotoFin == true)
            {
                transform.Translate(0, 0, Speed * Time.deltaTime);

            }
            else GotoFin = false;
            if (Vector3.Distance(transform.position, start33) > dif && GotoFin == false)
            {
                transform.Translate(0, 0, -Speed * Time.deltaTime);

            }
            else GotoFin = true;
        }

    }
}
