﻿using UnityEngine;
using UnityEngine.UI;

public class BusSpawn : MonoBehaviour
{
    [SerializeField] private Transform start1;
    private Vector3 start;
    private Vector3 finish;
    private Vector3 start22;
    private Vector3 finish22;
    private Vector3 start33;
    private Vector3 finish33;

    [SerializeField] private Transform finish1;
    [SerializeField] private Transform start2;
    [SerializeField] private Transform start3;

    [SerializeField] private Transform finish2;
    [SerializeField] private Transform finish3;
    [SerializeField] private GameObject Bus;
    [SerializeField] private float Speed = 10;
    [SerializeField] private Text BusPoolTxt;
    [SerializeField] private Text Row1Txt;
    [SerializeField] private Text Row2Txt;
    [SerializeField] private Text Row3Txt;
    [SerializeField]
    private float dif = 0.1f;
    bool GotoFin = true;
    Transform currentBus;
    private int i = 0;
    private int pool = 0;
    private int row1 = 0;
    private int row2 = 0;
    private int row3 = 0;
    public void Row1()
    {
        if (i < BussPool.Instance.BusPool.Length - 1)
        {
            currentBus.transform.position = start1.transform.position;
            i++;
            row1++;
            pool--;
        }
    }
    public void Row2()
    {
        if (i < BussPool.Instance.BusPool.Length - 1)
        {
            currentBus.transform.position = start2.transform.position;
            i++;
            row2++;
            pool--;
        }
    }
    public void Row3()
    {
        if (i < BussPool.Instance.BusPool.Length - 1)
        {
            currentBus.transform.position = start3.transform.position;
            i++;
            row3++;
            pool--;
        }
    }
    void Start()
    {

        pool = BussPool.Instance.BusPool.Length - 1;
       
    }

    void Update()
    {
        BusPoolTxt.text = "Buses in Park left: " + pool;
        Row1Txt.text = "Buses on Row 1: " + row1;
        Row2Txt.text = "Buses on Row 2: " + row2;
        Row3Txt.text = "Buses on Row 3: " + row3;
        currentBus = BussPool.Instance.BusPool[i].transform;
       
    }
}
