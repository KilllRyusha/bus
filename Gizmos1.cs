﻿using UnityEngine;

public class Gizmos1 : MonoBehaviour
{
    [SerializeField] private Transform start;
    [SerializeField] private Transform finish;
    [SerializeField] private Transform start2;
    [SerializeField] private Transform finish2;
    [SerializeField] private Transform start3;
    [SerializeField] private Transform finish3;
    [SerializeField] [Range(0, 2)] private float radius = 0.5f;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(start.position, radius);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(finish.position, radius);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(start2.position, radius);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(finish2.position, radius);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(start3.position, radius);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(finish3.position, radius);
    }
}
